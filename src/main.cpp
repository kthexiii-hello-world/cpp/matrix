#include <ncurses.h>

#include <bit>
#include <chrono>
#include <clocale>
#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <thread>
#include <vector>

#define _USE_MATH_DEFINES
#include <cmath>

class Sprite {
   public:
    enum class Color : int16_t {
        TRANSPARENT = -1,
        NONE = 0,
        NORMAL = 1,
    };

   public:
    Sprite()
        : m_Width(0),
          m_Height(0),
          m_DataBuffer(nullptr),
          m_ColorBuffer(nullptr) {}

    Sprite(int32_t const& width, int32_t const& height)
        : m_Width(width),
          m_Height(height),
          m_DataBuffer(nullptr),
          m_ColorBuffer(nullptr) {
        init(m_Width, m_Height);
    }

    ~Sprite() {
        delete[] m_DataBuffer;   // Free up the buffer
        delete[] m_ColorBuffer;  // Free up the color buffer
    }

    auto init(int32_t const& width, int32_t const& height) -> void {
        m_Width = width;
        m_Height = height;

        // Allocate memory for the buffer and color
        m_DataBuffer = new wchar_t[m_Width * m_Height];
        m_ColorBuffer = new Color[m_Width * m_Height];
    }

   public:
    inline auto set(int32_t const& x, int32_t const& y, wchar_t const& data,
                    Color const& color) -> void {
        if (y > -1 && y < m_Height && x > -1 && x < m_Width) {
            const auto index = y * m_Width + x;
            m_DataBuffer[index] = data;
            m_ColorBuffer[index] = color;
        }
    }
    inline auto getColor(int32_t const& x, int32_t const& y) const -> Color {
        if (y > -1 && y < m_Height && x > -1 && x < m_Width)
            return m_ColorBuffer[y * m_Width + x];
        else
            return Color::TRANSPARENT;
    }
    inline auto getData(int32_t const& x, int32_t const& y) const -> wchar_t {
        if (y > -1 && y < m_Height && x > -1 && x < m_Width)
            return m_DataBuffer[y * m_Width + x];
        else
            return L' ';
    }
    inline auto getColorBuffer() const { return m_ColorBuffer; }
    inline auto getBuffer() const { return m_DataBuffer; }
    inline auto getWidth() const { return m_Width; }
    inline auto getHeight() const { return m_Height; }

   private:
    int32_t m_Width;
    int32_t m_Height;
    wchar_t* m_DataBuffer;
    Color* m_ColorBuffer;
};

class Display {
   public:
    Display()
        : m_Window(nullptr), m_Height(0), m_Width(0), m_Initialized(false) {}

    ~Display() {
        endwin();  // Clean up the ncurses
    }

    auto init() -> void {
        if (m_Initialized) return;

        std::setlocale(LC_ALL, "");

        // Initialize the screen
        m_Window = initscr();
        // Get the terminal size in character
        getmaxyx(stdscr, m_Height, m_Width);

        m_Buffer.init(m_Width, m_Height);

        // Enable colors
        use_default_colors();  // Use the user default terminal color
        start_color();         // Start the color

        if (!has_colors() || !can_change_color())
            throw std::runtime_error("Terminal does not support color");

        init_color(60, 1000, 0, 1000);
        //       init_pair(1, -1, -1);
        init_pair(1, COLOR_WHITE, -1);

        m_Initialized = true;
    }

    inline auto clear() -> void {
        for (int32_t i = 0; i < m_Height; i++) {
            for (int32_t j = 0; j < m_Width; j++) {
                m_Buffer.set(j, i, ' ', Sprite::Color::NONE);
            }
        }
    }

    inline auto draw(Sprite const& sprite, int32_t const& x = 0,
                     int32_t const& y = 0) {
        for (int32_t i = 0; i < sprite.getHeight(); i++) {
            for (int32_t j = 0; j < sprite.getWidth(); j++) {
                const auto data = sprite.getData(j, i);
                const auto color = sprite.getColor(j, i);
                if (color != Sprite::Color::TRANSPARENT)
                    m_Buffer.set(j + x, i + y, data, color);
            }
        }
    }

    inline auto print(int32_t const& x, int32_t const& y,
                      std::string const& text,
                      Sprite::Color const& color = Sprite::Color::NONE)
        -> void {
        if (y > -1 && y < m_Height && x > -1 && x < m_Width) {
            int32_t row = 0;
            int32_t col = 0;
            for (int32_t i = 0; i < text.size(); i++) {
                auto character = text[i];
                m_Buffer.set(col + x, row + y, character, color);
                col++;

                if (character == '\n') {
                    row++;
                    col = 0;
                }
            }
        }
    }

    inline auto show() const -> void {
        attron(COLOR_PAIR(1));
        const auto buffer = m_Buffer.getBuffer();
        const auto colorBuffer = m_Buffer.getColorBuffer();

        for (int32_t i = 0; i < m_Height; i++) {
            for (int32_t j = 0; j < m_Width; j++) {
                move(i, j);
                addch(m_Buffer.getData(j, i));
            }
        }
        attroff(COLOR_PAIR(1));
        refresh();
    }

    auto showCursor(bool const& show) const { curs_set(show); }
    auto setInputTimeout(int32_t const& time = 0) const { timeout(time); }
    auto getInput() -> int32_t const { return getch(); }

    inline auto getRows() const { return m_Height; }
    inline auto getCols() const { return m_Width; }

   private:
    int32_t m_Height, m_Width;
    bool m_Initialized;

    WINDOW* m_Window;
    Sprite m_Buffer;
};

class Matrix {
   public:
    Matrix(int32_t const& rows, int32_t const& cols)
        : m_Rows(rows), m_Cols(cols) {
        // Set the random generator seed to current time
        std::srand(std::time(nullptr));
        m_Sprite.init(m_Cols, m_Rows);
    }

    ~Matrix() {}

    auto update() -> void {
        for (int32_t i = 0; i < m_Rows; i++) {
            for (int32_t j = 0; j < m_Cols; j++) {
                m_Sprite.set(j, i, 33 + (std::rand() % 80),
                             Sprite::Color::NONE);
            }
        }
    }

    inline auto getRows() const { return m_Rows; }
    inline auto getCols() const { return m_Cols; }

    inline auto& getSprite() const { return m_Sprite; }

   private:
    int32_t m_Rows, m_Cols;
    Sprite m_Sprite;
};

auto main() -> int32_t {
    auto display = Display();
    try {
        display.init();
    } catch (std::exception const& e) {
        std::cerr << e.what() << "\n";
        return EXIT_FAILURE;
    }

    display.showCursor(false);
    display.setInputTimeout();

    auto rows = display.getRows();
    auto cols = display.getCols();

    auto matrix = Matrix(rows, cols);
    auto isRunning = true;

    auto sprite = Sprite(30, 15);
    for (int32_t i = 0; i < 15; i++) {
        for (int32_t j = 0; j < 30; j++) {
            sprite.set(j, i, L'D', Sprite::Color::NONE);
        }
    }

    auto ballSprite = Sprite(32, 16);
    const auto maxRadius = ballSprite.getHeight() / 2.0;

    for (int32_t i = 0; i < ballSprite.getHeight(); i++) {
        for (int32_t j = 0; j < ballSprite.getWidth(); j++) {
            double x = (double)j / 2.0 - ballSprite.getWidth() / 4.0;
            double y = (double)i - ballSprite.getHeight() / 2.0;

            double radius = std::sqrt(x * x + y * y);

            if (radius <= maxRadius)
                ballSprite.set(j, i, L'A', Sprite::Color::NONE);
            else
                ballSprite.set(j, i, ' ', Sprite::Color::TRANSPARENT);
        }
    }

    double x = 10, y = 50;
    double speed = 0.5;
    double xd = 1, yd = 1;

    while (isRunning) {
        const auto input = display.getInput();
        if (input == 'q') isRunning = false;

        display.clear();  // Clear the screen buffer
        // display.draw(matrix.getSprite());
        display.draw(sprite, 10, 10);
        display.draw(sprite, 25, 40);
        display.draw(sprite, 70, 40);
        display.draw(ballSprite, (int32_t)x, (int32_t)y);
        display.print(0, 0, curses_version());
        display.print(0, 1, "Press \'q\' to exit");

        if (x < 0 || x + maxRadius * 4.3 > cols) xd = -xd;
        if (y < 0 || y + maxRadius * 2.0 > rows) yd = -yd;

        x += speed * xd;
        y += speed * yd;

        // Show the data onto the display
        display.show();

        using namespace std::chrono_literals;
        std::this_thread::sleep_for(16ms);
    }

    return EXIT_SUCCESS;
}
