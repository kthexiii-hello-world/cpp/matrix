# matrix in C++20

## Requirements

- clang11

## How to run

```shell
make release
```

The binary should be in `bin` folder under the name `matrix`

```shell
bin/matrix
```

