CXX = clang++
CFLAGS := -std=c++2a -fmodules-ts
CFLIB := ""

# Get OS, only for macOS and Linux
UNAME_S = $(shell uname -s)
ifeq ($(UNAME_S),Linux)
endif
ifeq ($(UNAME_S),Darwin)
	CFLIB += -L/usr/local/opt/ncurses/lib -I/usr/local/opt/ncurses/include
endif

CFLIB += -lncursesw

all: debug

debug: create_bin_obj create_debug bin/debug/matrix
release: create_bin_obj bin/matrix

clean:
	@echo "Cleaning bin and obj..." && rm -rf bin/* obj/*

bin/debug/matrix: src/main.cpp
	@echo "Builing Debug..."
	@$(CXX) $(CFLAGS) $(CFLIB) -g -O0 -o $@ -fprebuilt-module-path=obj $^
	@echo "Done..."

bin/matrix: src/main.cpp
	@echo "Builing..."
	@$(CXX) $(CFLAGS) $(CFLIB) -o $@ -fprebuilt-module-path=obj $^
	@echo "Done..."

# obj/sprite.pcm: src/sprite.cpp
# 	@$(CXX) $(CFLAGS) -o $@ --precompile -x c++-module $^

# obj/display.pcm: src/display.cpp
# 	@$(CXX) $(CFLAGS) -c $@ --precompile -x c++-module -fprebuilt-module-path=obj $^

create_bin_obj:
	@mkdir -p bin
	@mkdir -p obj

create_debug:
	@mkdir -p bin/debug
	@mkdir -p obj/debug
